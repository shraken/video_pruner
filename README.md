# Introduction

Looks at all files and files contained in subdirectories within
a directory for a reference watermark.  If the watermark is found,
then the video file is cut at the time location the watermark is found.

The program uses the following method in analyzing frames:
 - simple pixel-by-pixel correlation
 - histogram correlation
 - number of matched homography points

These inputs are fed into a decision method to decide if the watermark
frame matches the reference image.  

# Requirements

pip install opencv-contrib-python
pip install moviepy

# Usage

sage: process_dir.py [-h] [-i,--input_dir INOUT_DIR]
                      [-r,--reference_file REF_FILE]

Scan directory for all video files and cut out watermarks

optional arguments:
  -h, --help            show this help message and exit
  -i,--input_dir INOUT_DIR
                        input directory to scan for video files
  -r,--reference_file REF_FILE
                        reference image file to use for comparison