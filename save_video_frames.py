'''
    @brief 
    @author shraken
    @date 8/26/2018
'''

import cv2
import argparse
import sys
import os
import ntpath

SAVE_FILE_EXTENSION = '.jpg'

def getNumberFrames(filename):
    cap = cv2.VideoCapture(filename)

    if cap.isOpened():
        return int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    else:
        return -1

def getFPS(filename):
    cap = cv2.VideoCapture(filename)

    if cap.isOpened():
        # Find OpenCV version
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
     
        if int(major_ver)  < 3 :
            fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
            print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
            return fps
        else:
            fps = cap.get(cv2.CAP_PROP_FPS)
            print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
            return fps
    return 0

def generateImageByIndex(src_filename, dst_filename, index):
    print('dst_filename = {}'.format(dst_filename))
    print('index = {}'.format(index))

    cap = cv2.VideoCapture(src_filename)

    #cap.set(2, float(index))
    # @todo: find a better way to set framepos
    for i in range(0, int(index)):
        res, img = cap.read()

    cv2.imwrite(dst_filename, img)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extract all frame images from a video file and save them')

    parser.add_argument('-i,--input_file', dest='input_file',
                        help='input video to open for image extraction', required=True)
    parser.add_argument('-o,--output_dir', dest='output_dir',
                        help='output directory to save images to', required=True)

    args = parser.parse_args()

    numberFrames = getNumberFrames(args.input_file)
    framePerSecond = getFPS(args.input_file)

    print('Processing video file {}'.format(args.input_file))
    print('numberFrames = {}'.format(numberFrames))
    print('framePerSecond = {}'.format(framePerSecond))

    # extract filename
    baseFileName = ntpath.basename(args.input_file)
    filename, file_extension = os.path.splitext(baseFileName)

    print('baseFileName = {}'.format(baseFileName))
    print('filename = {}'.format(filename))
    print('file_extension = {}'.format(file_extension))

    cap = cv2.VideoCapture(args.input_file)

    for index in range(0, numberFrames):
        res, img = cap.read()

        dst_filename = '{}/{}{}'.format(args.output_dir, index, SAVE_FILE_EXTENSION)
        print('dst_filename = {}'.format(dst_filename))
        cv2.imwrite(dst_filename, img)