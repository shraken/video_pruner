#!/usr/bin/env python

'''
    Feature homography maps characteristics of two photos.  We show a line
    between the two indicating the matches.  

    8/23/2018
'''

import numpy as np
import cv2
import sys
import time
from matplotlib import pyplot as plt

def checkImageVideo(imgin, ref_file):
    MINIMUM_KEYPOINT_THRESH = 10
    
    imgref_wm = cv2.imread(ref_file)

    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(imgref_wm, None)
    kp2, des2 = sift.detectAndCompute(imgin, None)

    if (des1 is None) or (des2 is None):
        return 0

    if (len(des1) == 0) or (len(des2) == 0):
        return 0

    if (len(kp1) < MINIMUM_KEYPOINT_THRESH) or (len(kp2) < MINIMUM_KEYPOINT_THRESH):
        return 0

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1, des2, k = 2)

    good = []
    for m,n in matches:
        if m.distance < (0.7 * n.distance):
            good.append(m)
    return len(good)

def checkImage(in_filename, ref_file):
    MINIMUM_KEYPOINT_THRESH = 10
    
    imgref_wm = cv2.imread(ref_file)
    imgin = cv2.imread(in_filename)

    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(imgref_wm, None)
    kp2, des2 = sift.detectAndCompute(imgin, None)

    if (des1 is None) or (des2 is None):
        return 0

    if (len(des1) == 0) or (len(des2) == 0):
        return 0

    if (len(kp1) < MINIMUM_KEYPOINT_THRESH) or (len(kp2) < MINIMUM_KEYPOINT_THRESH):
        return 0

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1, des2, k = 2)

    good = []
    for m,n in matches:
        if m.distance < (0.7 * n.distance):
            good.append(m)
    return len(good)

def checkVideo(in_filename, out_filename, ref_file, verbose_mode=False):
    imgref_wm = cv2.imread(ref_file)
    cap = cv2.VideoCapture(in_filename)

    w_in = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h_in = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    cap_ref = cv2.VideoCapture(ref_file)
    w_ref = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h_ref = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    cap_ref.release()

    imgref_wm_resize = cv2.resize(imgref_wm, (w_in, h_in)) 

    fps = 0
    if cap.isOpened():
        # Find OpenCV version
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
     
        if int(major_ver)  < 3 :
            fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
            if verbose_mode:
                print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
        else :
            fps = cap.get(cv2.CAP_PROP_FPS)
            if verbose_mode:
                print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
    
    if fps == 0:
        print('ERROR: could not determine fps of source video file')
        sys.exit(-1)

    framelen = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # check over input file and find the frames that should be cut
    count = 0
    local_count = 0
    corr_check_max = 0

    ret = True

    corrFrames = []
    count = 0

    while ret:
        if verbose_mode:
            print('feature_homography: Checking frame {}/{}'.format(count, framelen))
        ret, frame = cap.read()

        if ret is False:
            break

        value = checkImageVideo(frame, ref_file)
        
        corrFrames.append(value)
        count += 1

    cap.release()
    return corrFrames