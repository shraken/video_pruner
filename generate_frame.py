'''
    @brief User specifies a time index to extract a video frame.  The video frame
    is saved to disk and used in subsequent processing with `splice_videos.py`
    script.  

    @author shraken
    @date 7/28/2018
'''

import cv2
import argparse
import sys

def getNumberFrames(filename):
    cap = cv2.VideoCapture(filename)

    if cap.isOpened():
        return int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    else
        return -1

def getFPS(filename):
    cap = cv2.VideoCapture(filename)

    if cap.isOpened():
        # Find OpenCV version
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
     
        if int(major_ver)  < 3 :
            fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
            print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
            return fps
        else :
            fps = cap.get(cv2.CAP_PROP_FPS)
            print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
            return fps
    return 0

def generateImageByIndex(src_filename, dst_filename, index):
    print('dst_filename = {}'.format(dst_filename))
    print('index = {}'.format(index))

    cap = cv2.VideoCapture(src_filename)

    #cap.set(2, float(index))
    # @todo: find a better way to set framepos
    for i in range(0, int(index)):
        res, img = cap.read()

    cv2.imwrite(dst_filename, img)

def generatImageByTime(src_filename, dst_filename, time):
    cap = cv2.VideoCapture(src_filename)

    print('time = {}'.format(time))

    fps = float(getFPS(src_filename))
    index = (float(time) * fps)
    print('index = {}'.format(index))

    generateImageByIndex(src_filename, dst_filename, int(index))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extract frame image from video files')

    parser.add_argument('-i,--index', dest='video_index_cut',
                        help='frame index at which to cut file')
    parser.add_argument('-t,--time', dest='video_time_cut',
                        help='frame time at which to cut file')
    parser.add_argument('-f,--input_file', dest='input_file',
                        help='input video to open for image extraction')
    parser.add_argument('-o,--output_file', dest='output_file',
                        help='output image the extraction is saved to')

    args = parser.parse_args()

    if (not args.video_time_cut) and (not args.video_index_cut):
        parser.print_help()
        sys.exit(1)

    if args.video_index_cut:
        generateImageByIndex(args.input_file, args.output_file, args.video_index_cut)
    elif args.video_time_cut:
        generatImageByTime(args.input_file, args.output_file, args.video_time_cut)