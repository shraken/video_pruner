'''
    @brief Checks video file image frames to see if they match a known frame.  The
    script then determines the location of the matching frame and cuts the file
    from t = 0 sec to the time where the watermark is found.  The script uses a
    correlation on grayscale images to check if the watermark is found.  

    @author shraken
    @date 7/28/2018
'''

import cv2
import sys
import os.path
import time
import numpy as np
import math
import argparse
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip

DEFAULT_THRESHOLD_CORR_CHECK = 0.9

corr_check_max = 0

def mean2(x):
    y = np.sum(x) / np.size(x)
    return y

def corr2(a,b):
    a = a - mean2(a)
    b = b - mean2(b)

    r = (a*b).sum() / math.sqrt((a*a).sum() * (b*b).sum())
    return r

def checkRef(frame, imgref):
    imgref_gray = cv2.cvtColor(imgref, cv2.COLOR_BGR2GRAY)
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    corr_check = corr2(imgref_gray, frame_gray)

    if (corr_check > DEFAULT_THRESHOLD_CORR_CHECK):
        return (True, corr_check)
    return (False, corr_check)

def generateVideo(in_filename, out_filename, t_time_wm):
    ffmpeg_extract_subclip(in_filename, 0, t_time_wm, targetname=out_filename)

def checkImage(in_filename, ref_file):
    imgref_wm = cv2.imread(ref_file)
    imgin = cv2.imread(in_filename)

    h_in, w_in, c_in = imgin.shape
    h_wm, w_wm, c_wm = imgref_wm.shape

    imgref_wm_resize = cv2.resize(imgref_wm, (w_in, h_in)) 
    status, value = checkRef(imgin, imgref_wm_resize)

    return value

def checkVideo(in_filename, out_filename, ref_file, verbose_mode=False):
    imgref_wm = cv2.imread(ref_file)
    cap = cv2.VideoCapture(in_filename)

    w_in = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h_in = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    cap_ref = cv2.VideoCapture(ref_file)
    w_ref = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h_ref = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    cap_ref.release()

    imgref_wm_resize = cv2.resize(imgref_wm, (w_in, h_in)) 

    fps = 0
    if cap.isOpened():
        # Find OpenCV version
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
     
        if int(major_ver)  < 3 :
            fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
            if verbose_mode:
                print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
        else :
            fps = cap.get(cv2.CAP_PROP_FPS)
            if verbose_mode:
                print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
    
    if fps == 0:
        print('ERROR: could not determine fps of source video file')
        sys.exit(-1)

    framelen = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # check over input file and find the frames that should be cut
    count = 0
    local_count = 0
    corr_check_max = 0

    ret = True

    corrFrames = []
    count = 0

    while ret:
        if verbose_mode:
            print('image_correlate: Checking frame {}/{}'.format(count, framelen))
        ret, frame = cap.read()

        if ret is False:
            break

        status, value = checkRef(frame, imgref_wm_resize)

        '''
        if status is True:
            t_index_wm = count
            t_time_wm = (t_index_wm * (1.0 / fps))

            print('Found watermark at time t = {:0.2f} seconds'.format(t_time_wm))
            print('with an image_correlate of {}'.format(value))
            print('Generating video {} with {} frames'.format(out_filename, count))
            
            #generateVideo(in_filename, out_filename, t_time_wm)
            break
        '''

        corrFrames.append(value)
        count += 1

    cap.release()
    return corrFrames

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Strip watermark trailer off of video file')

    parser.add_argument('-f,--input_file', dest='input_file',
                        help='input video to strip the watermark off')
    parser.add_argument('-o,--output_file', dest='output_file',
                        help='output video file with trailer video stripped')
    parser.add_argument('-r,--reference_file', dest='ref_file',
                        help='reference image file to use for comparison')

    args = parser.parse_args()

    if ((not args.input_file) and (not args.ref_file) and (not args.output_file)):
        parser.print_help()
        sys.exit(1)

    checkVideo(args.input_file, args.output_file, args.ref_file)