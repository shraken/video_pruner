#!/usr/bin/env python

'''

    8/23/2018
'''

import matplotlib.pyplot as plt
import numpy as np
import argparse
import cv2
import os
import sys

validFileExt = [ '.jpg', '.jpeg', '.png' ]

def quickCalcHist(imagePath):
    image = cv2.imread(imagePath)
    imageCvt = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    
    hist = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8],
		[0, 256, 0, 256, 0, 256])

    cv2.normalize(hist, hist)
    hist = hist.flatten()
    return hist

def quickCalcHistVideo(image):
    imageCvt = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    
    hist = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8],
		[0, 256, 0, 256, 0, 256])

    cv2.normalize(hist, hist)
    hist = hist.flatten()
    return hist

def checkImage(in_filename, ref_file):
    return checkFile(in_filename, ref_file)

def checkFileVideo(frame, ref_file):
    hist_ref = quickCalcHist(ref_file)
    hist_in = quickCalcHistVideo(frame)

    d = cv2.compareHist(hist_ref, hist_in, cv2.HISTCMP_CORREL)
    return d

def checkFile(in_filename, ref_file):
    hist_ref = quickCalcHist(ref_file)
    hist_in = quickCalcHist(in_filename)

    d = cv2.compareHist(hist_ref, hist_in, cv2.HISTCMP_CORREL)
    return d

def checkVideo(in_filename, out_filename, ref_file, verbose_mode=False):
    imgref_wm = cv2.imread(ref_file)
    cap = cv2.VideoCapture(in_filename)

    w_in = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h_in = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    cap_ref = cv2.VideoCapture(ref_file)
    w_ref = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h_ref = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    cap_ref.release()

    imgref_wm_resize = cv2.resize(imgref_wm, (w_in, h_in)) 

    fps = 0
    if cap.isOpened():
        # Find OpenCV version
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
     
        if int(major_ver)  < 3 :
            fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
            if verbose_mode:
                print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
        else :
            fps = cap.get(cv2.CAP_PROP_FPS)
            if verbose_mode:
                print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
    
    if fps == 0:
        print('ERROR: could not determine fps of source video file')
        sys.exit(-1)

    framelen = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # check over input file and find the frames that should be cut
    count = 0
    local_count = 0
    corr_check_max = 0

    ret = True

    corrFrames = []
    count = 0

    while ret:
        if verbose_mode:
            print('hist_compare: Checking frame {}/{}'.format(count, framelen))
        ret, frame = cap.read()

        if ret is False:
            break

        value = checkFileVideo(frame, ref_file)
        
        corrFrames.append(value)
        count += 1

    cap.release()
    return corrFrames

def processFiles(walk_dir, out_dir, ref_file):
    for root, subdirs, files in os.walk(walk_dir):
        for filename in files:
            file_path = os.path.join(root, filename)

            print('\t- file %s (full path: %s)' % (filename, file_path))
            filename, file_extension = os.path.splitext(file_path)

            print('filename = {}'.format(filename))
            print('file_extension = {}'.format(file_extension))
  
            rootFileList = file_path.split('/')
            rootFile = rootFileList[-1]

            if file_extension in validFileExt:
                print('VALID file extension found in file {}'.format(file_path))

                output_filename = out_dir + '/' + rootFile
                value = checkFile(file_path, ref_file)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compute correlation coefficent of image histograms')

    parser.add_argument('-i,--input_dir', dest='in_dir',
                        help='input directory to scan for video files')
    parser.add_argument('-o--out_dir', dest='out_dir',
                        help='output directory to write new video files to')
    parser.add_argument('-r,--reference_file', dest='ref_file',
                        help='reference image file to use for comparison')

    args = parser.parse_args()

    if ((not args.in_dir) or (not args.out_dir) or (not args.ref_file)):
        parser.print_help()
        sys.exit(1)

    processFiles(str(args.in_dir), str(args.out_dir), str(args.ref_file))

