'''
    @brief Matches a watermark reference photo against
    multiple images in a directory.  The pixel-by-pixel
    image correlation, histogram correlation, and a
    feature homography detection count is computed by
    comparing the reference image to each image in the
    scan directory.

    The resulting correlatiom and feature detections are
    saved to a csv file for post analysis.  
    
    @author shraken
    @date 8/31/2018
'''

import argparse
import os
import sys
import cv2
import csv
import image_correlate as ic
import hist_compare as hc
import feature_homography as fh

validFileImageExt = [ '.jpg', '.jpeg', '.png', '.gif' ]

def getFPS(filename):
    cap = cv2.VideoCapture(filename)

    if cap.isOpened():
        # Find OpenCV version
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
     
        if int(major_ver)  < 3 :
            fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
            print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
            return fps
        else :
            fps = cap.get(cv2.CAP_PROP_FPS)
            print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
            return fps
    return 0

def writeData(fps, records, out_dir):
    # write records to disk
    with open(out_dir + '/records.csv', 'wb') as f:
        process_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        process_writer.writerow(['index', 'time', 'imageCorrelate', 
                                 'histogramCorrelate', 'featureHomographyCount'])

        # iterate over all image frame refs
        for index, value in records.iteritems():
            time = (1.0 / fps) * index
            process_writer.writerow(['{}'.format(index),
                                     '{}'.format(time),
                                     '{}'.format(value[0]), 
                                     '{}'.format(value[1]),
                                     '{}'.format(value[2])])

def processImageFiles(walk_dir, out_dir, ref_file, matchExt):
    print('walk_dir = {}'.format(walk_dir))
    print('ref_file = {}'.format(ref_file))

    records = {}
    for root, subdirs, files in os.walk(walk_dir):
        for filename in files:
            file_path = os.path.join(root, filename)
            fps = getFPS(file_path)

            print('checking file %s (full path: %s)' % (filename, file_path))
            print('fps = {}'.format(fps))

            filename, file_extension = os.path.splitext(file_path)

            rootFileList = file_path.split('/')
            rootFile = rootFileList[-1]

            # get the file index as the filename in the input directory
            # "1.jpeg" => 1
            actualFileIndex = rootFile.split('.')[0]

            if file_extension in matchExt:
                output_filename = out_dir + '/' + rootFile

                imageCorrelate = ic.checkImage(file_path, ref_file)
                histogramCorrelate = hc.checkImage(file_path, ref_file)
                featureHomographyCount = fh.checkImage(file_path, ref_file)

                print('actualFileIndex = {}'.format(actualFileIndex))
                print('imageCorrelate = {}'.format(imageCorrelate))
                print('histogramCorrelate = {}'.format(histogramCorrelate))
                print('featureHomographyCount = {}'.format(featureHomographyCount))

                records[int(actualFileIndex)] = (imageCorrelate, histogramCorrelate, featureHomographyCount)

    writeData(fps, records, out_dir)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Scan directory for all video files and cut out watermarks')

    parser.add_argument('-i,--input_dir', dest='in_dir',
                        help='input directory to scan for video files')
    parser.add_argument('-o--out_dir', dest='out_dir',
                        help='output directory to write new video files to')
    parser.add_argument('-r,--reference_file', dest='ref_file',
                        help='reference image file to use for comparison')

    args = parser.parse_args()

    if ((not args.in_dir) or (not args.out_dir) or (not args.ref_file)):
        parser.print_help()
        sys.exit(1)

    processImageFiles(str(args.in_dir), str(args.out_dir), str(args.ref_file), validFileImageExt)