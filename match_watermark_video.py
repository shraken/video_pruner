'''
    @brief Matches a watermark reference photo against
    multiple video files in a directory.  We run a 
    pixel-by-pixel image correlation, histogram correlation, 
    and a feature homography detection count comparing
    the reference image against each frame of the video
    files.  

    The resulting correlatiom and feature detections are
    saved to a csv file for post analysis.  
    
    @author shraken
    @date 8/31/2018
'''

import argparse
import os
import sys
import cv2
import csv
import itertools
import image_correlate as ic
import hist_compare as hc
import feature_homography as fh
from shutil import copyfile
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from scipy.signal import butter, lfilter, freqz
import moviepy.editor as mp

# number of frames to let pass before rejecting further watermarks
THRESHOLD_FALSE_POSITIVE = 250 # frames

# normalized coefficient (-1 to 1) threshold to detect watermark
DEFAULT_THRESHOLD_IMAGE_DETECT = 0.9

# normalized frame index position marker for start and end watermark detection
THRESHOLD_START_END_WATERMARK = 0.5

validFileVideoExt = [ '.mp4', '.avi', '.divx', '.wmv', '.avi', '.mkv', 'mov']

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def writeData(fps, records, filepath):
    # write records to disk
    with open(filepath, 'wb') as f:
        process_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        process_writer.writerow(['index', 'time', 'imageCorrelate', 
                                 'histogramCorrelate', 'featureHomographyCount'])

        count = 0
        for imgCor, histCor, featHomo in \
            itertools.izip(records['imageCorrRecords'], 
                           records['histogramCorrelate'], 
                           records['featureHomographyCount']):
            
            time = (1.0 / fps) * count
            process_writer.writerow(['{}'.format(count),
                                     '{}'.format(time),
                                     '{}'.format(imgCor), 
                                     '{}'.format(histCor),
                                     '{}'.format(featHomo)])

            count += 1

def getFPS(filename):
    cap = cv2.VideoCapture(filename)

    if cap.isOpened():
        # Find OpenCV version
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
     
        if int(major_ver)  < 3 :
            fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
            print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
            return fps
        else :
            fps = cap.get(cv2.CAP_PROP_FPS)
            print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
            return fps
    return 0

def convertVideo(source_file, dest_file):
    DEFAULT_HEIGHT = 360

    clip = mp.VideoFileClip(source_file)
    clip_resized = clip.resize(height=DEFAULT_HEIGHT)
    clip_resized.write_videofile(dest_file)

def cutVideo(file_path, outVideoFile, t1, t2):
    ffmpeg_extract_subclip(file_path, t1, t2, targetname=outVideoFile)
    print('Video File was saved as {}'.format(outVideoFile))
    print('We cut from t = 0 sec to t1 = {} sec'.format(t1))
    print('with a total movie length of {} sec'.format(t2))

# taken from: 
# https://stackoverflow.com/questions/25191620/creating-lowpass-filter-in-scipy-understanding-methods-and-units
def filterImageDetect(loadedData):
    # Filter requirements.
    order = 2
    fs = 60.0       # sample rate, Hz
    cutoff = 5      # desired cutoff frequency of the filter, Hz

    # Get the filter coefficients so we can check its frequency response.
    b, a = butter_lowpass(cutoff, fs, order)

    fs = 60.0
    y = butter_lowpass_filter(loadedData, cutoff, fs, order)
    return y

def processVideoFiles(walk_dir, out_dir, ref_file, matchExt, verbose_mode=False, resize_mode=False):
    records = {}
    for root, subdirs, files in os.walk(walk_dir):
        for orig_filename in files:
            file_path = os.path.join(root, orig_filename)
            fps = getFPS(file_path)
 
            print('checking file %s (full path: %s)' % (orig_filename, file_path))
            filename, file_extension = os.path.splitext(file_path)

            rootFileList = file_path.split('/')
            rootFile = rootFileList[-1]

            actualFileIndex = rootFile.split('.')[0]
            outCsvFile = out_dir + '/' + orig_filename + '_record.csv'
            outVideoFile = out_dir + '/' + orig_filename

            if file_extension in matchExt:
                output_filename = out_dir + '/' + rootFile

                if resize_mode:
                    conv_file_path = out_dir + '/' + orig_filename + '_conv.mp4'
                    convertVideo(file_path, conv_file_path)
                    oper_file = conv_file_path
                else:
                    oper_file = file_path
                    
                icTemp = ic.checkVideo(oper_file, output_filename, ref_file, verbose_mode)
                records['imageCorrRecords'] = icTemp
                
                hcTemp = hc.checkVideo(oper_file, output_filename, ref_file, verbose_mode)
                records['histogramCorrelate'] = hcTemp

                #fhTemp = fh.checkVideo(oper_file, output_filename, ref_file, verbose_mode)
                fhTemp = [0] * len(hcTemp)
                records['featureHomographyCount'] = fhTemp

                # split to look for start and end of watermark
                splitIndex = int(len(icTemp) * THRESHOLD_START_END_WATERMARK)
                
                startIcTemp = icTemp[0:splitIndex]
                endIcTemp = icTemp[(splitIndex + 1):]

                startHcTemp = hcTemp[0:splitIndex] 
                endHcTemp = hcTemp[(splitIndex + 1):]

                # for start watermark, register only the last detection
                
                startIcTempLastIdx = -1
                startHcTempLastIdx = -1

                for index, value in enumerate(startIcTemp):
                    if (value > DEFAULT_THRESHOLD_IMAGE_DETECT):
                        startIcTempLastIdx = index

                for index, value in enumerate(startHcTemp):
                    if (value > DEFAULT_THRESHOLD_IMAGE_DETECT):
                        startHcTempLastIdx = index

                # for end watermark, register only the earliest detection
                detectedIcTemp = False
                detectedHcTemp = False
                endIcTempLastIdx = -1
                endHcTempLastIdx = -1

                for index, value in enumerate(endIcTemp):
                    if (value > DEFAULT_THRESHOLD_IMAGE_DETECT) and (detectedIcTemp == False):
                        endIcTempLastIdx = index
                        detectedIcTemp = True

                for index, value in enumerate(endHcTemp):
                    if (value > DEFAULT_THRESHOLD_IMAGE_DETECT) and (detectedHcTemp == False):
                        endHcTempLastIdx = index
                        detectedHcTemp = True

                if (startHcTempLastIdx != -1) and (endHcTempLastIdx != -1):
                    print('Cutting a start watermark at frame index {}'.format(index))

                    t1 = (1.0 / fps) * startHcTempLastIdx
                    t2 = (1.0 / fps) * endHcTempLastIdx

                    print('Found start and end of watermark')
                    print('Cutting video over [{},{}]'.format(t1, t2))
                elif startHcTempLastIdx != -1:
                    t1 = (1.0 / fps) * startHcTempLastIdx
                    t2 = (1.0 / fps) * len(icTemp)

                    print('Found watermark in begin of file')
                    print('Cutting video over [{},{}]'.format(t1, t2))
                elif endHcTempLastIdx != -1:
                    # adjust for the initial split index by adding the original base index
                    endHcTempLastIdx += splitIndex

                    t1 = (1.0 / fps) * 0
                    t2 = (1.0 / fps) * endHcTempLastIdx

                    print('Found watermark in end of file')
                    print('Cutting video over [{},{}]'.format(t1, t2))

                cutVideo(file_path, outVideoFile, t1, t2)
                writeData(fps, records, outCsvFile)
    print('All Done!')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Scan directory for all video files and cut out watermarks')

    parser.add_argument('-i,--input_dir', dest='in_dir',
                        help='input directory to scan for video files')
    parser.add_argument('-o--out_dir', dest='out_dir',
                        help='output directory to write new video files to')
    parser.add_argument('-r,--reference_file', dest='ref_file',
                        help='reference image file to use for comparison')
    parser.add_argument('-v,--verbose', action='store_true', dest='verbose_mode',
                        help='verbose console prints when processing')
    parser.add_argument('-x,--resize', action='store_true', dest='resize_mode',
                        help='resize and downscale video resolution before analysis')

    args = parser.parse_args()

    if ((not args.in_dir) or (not args.out_dir) or (not args.ref_file)):
        parser.print_help()
        sys.exit(1)

    processVideoFiles(str(args.in_dir), str(args.out_dir), str(args.ref_file), \
                      validFileVideoExt, args.verbose_mode, args.resize_mode)